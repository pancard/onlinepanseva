<div class="header" id="header">
        <div class="container-fluid mb-10px">
            <div class="row text-center">
                <div class="col">
                    <a href="/"><img class="header-left-image" src="assets/img/online-msme-logo.png" /></a>
                </div>
                <div class="col">
                    <img class="header-center-image mt-20px" src="assets/img/online-msme-center-image.png" />
                </div>
                <div class="col d-none d-md-block">
                    <img class="header-right-image" src="assets/img/shopact-msme-gst-swacha-bharat.png" />
                </div>
            </div>
        </div>
        <nav class="navbar navbar-expand-lg navbar-light bg-light-gray p-01rem fs-14">
            <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarsExample08" aria-controls="navbarsExample08" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="navbar-collapse justify-content-md-center collapse" id="navbarsExample08" style="">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="/">APPLY PAN</a>
                        
                    </li>
                    <!-- <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">CHANGE PAN</a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="change-correction-cancellation-pan-indian.html">Correction/Cancellation of Pan - Indian Citizen</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">REISSUE PAN</a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="reprint-reissue-lost-duplicate-pan-indian.html">Reprint/Reissue/Lost/Duplicate Pan - Indian Citizen</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">FOREIGNER CITIZEN </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="new-pan-foreigner.html">New Pan - Foreigner Citizen</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="change-correction-cancellation-pan-foreigner.html">Change/Correction/Cancellation of Pan - Foreigner Citizen</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="reprint-reissue-lost-duplicate-pan-foreigner.html">Reprint/Reissue/Lost/Duplicate Pan - Foreigner Citizen</a>
                        </div>
                    </li> -->
                    <!-- <li class="nav-item">
                        <a class="nav-link" href="/required-proofs">REQUIRED PROOFS</a>
                    </li> -->
                    <li class="nav-item">
                        <a class="nav-link" href="/faq">FAQ</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/contact">CONTACT US</a>
                    </li>
                </ul>
            </div>
        </nav>

            <div class="container-fluid bg-color-green ptb-1rem">
                <marquee class="mt-5px text-white">Online Portal For PAN Card Application - (Apply Online Easy & Simple Process)</marquee>
            </div>
        </div>

        <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>

<script src="assets/plugins/datepicker/bootstrap-datepicker.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script src="assets/js/script.js"></script>
<script>
        $("#enquiry-full-name").keyup(function(e) {
            var regex = new RegExp("^[a-zA-Z0-9 ]+$");
            var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
            if (regex.test(str)) return true;
            e.preventDefault();
            return false;
        });
    </script>