<footer class="foot-bottom">
            <div class="container-fluid mt-10px bg-color-green">
                <div class="col-md-12 row mrl-0px">
                    <div class="col-md-6 row p-20px mrl-0px">
                        <div class="col-md-12 pl-10px">
                            <h3 class="text-white">QUICK LINKS</h3>
                            <hr class="hr f-left" />
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-4 col-sm-12 col-xs-12 p-10px">
                            <a class="text-white td-none" href="/index">HOME</a>
                        </div>
                        <div class="col-md-4 col-sm-12 col-xs-12 p-10px">
                            <a class="text-white td-none" href="/index">INDIAN CITIZEN</a>
                        </div>
                        <!-- <div class="col-md-4 col-sm-12 col-xs-12 p-10px">
                            <a class="text-white td-none" href="/required-proofs" target="_blank">REQUIRED PROOFS</a>
                        </div> -->
                        <div class="col-md-4 col-sm-12 col-xs-12 p-10px">
                            <a class="text-white td-none" href="/faq">FAQ</a>
                        </div>
                        
                        <div class="col-md-4 col-sm-12 col-xs-12 p-10px">
                            <a class="text-white td-none" href="/contact">CONTACT US</a>
                        </div>
                    </div>
                    <div class="col-md-6 row p-20px mrl-0px">
                        <div class="col-md-12 pl-10px">
                            <div class="col-md-12 pl-10px">
                                <h3 class="text-white">PAYMENT METHODS</h3>
                                <hr class="hr f-left" />
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-12 mt-10px">
                                <img src="{{ asset('assets/img/pay/6.png') }}" style="width: 100px;" />
                                <img src="{{ asset('assets/img/pay/1.png') }}" style="width: 100px;" />
                                <img src="{{ asset('assets/img/pay/2.png') }}" style="width: 100px;" />
                                <img src="{{ asset('assets/img/pay/3.png') }}" style="width: 100px;" />
                                <img src="{{ asset('assets/img/pay/4.png') }}" style="width: 100px;" />
                                <img src="{{ asset('assets/img/pay/5.png') }}" style="width: 100px;" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid bg-color-orange">
                <div class="col-md-12 row mrl-0px p-20px">
                    <div class="col-md-7 col-sm-6 text-white row">
                        <a class="td-none text-white plr-5px col-xs-12" href="/terms-and-conditions">TERMS & CONDITIONS</a>
                        <a class="td-none text-white plr-5px col-xs-12" href="/privacy-policy">PRIVACY POLICY</a>
                        <a class="td-none text-white plr-5px col-xs-12" href="/disclaimer">DISCLAIMER</a>
                        <a class="td-none text-white plr-5px col-xs-12" href="/refund-policy">REFUND POLICY</a>
                        <a class="td-none text-white plr-5px col-xs-12" href="/about">ABOUT US</a>
                    </div>
                    <div class="col-md-5 justify-content-right d-none d-md-block">
                        <div class="text-right">
                            © 2019 Copyright <a class="td-none text-white fw-700" href="/"> Onlinepanseva </a> Powered By
                            <a href="/" target="_blank" class="text-white td-none fw-700">www.onlinepanseva.com</a>
                        </div>
                    </div>
                </div>
            </div>
        </footer>


        <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
        
        <link href="{{ asset('assets/plugins/datepicker/bootstrap-datepicker.js') }}" rel="stylesheet" type="text/js" />
        <link href="{{ asset('assets/js/bootstrap.min.js') }}" rel="stylesheet" type="text/js" />
        <link href="{{ asset('assets/js/script.js') }}" rel="stylesheet" type="text/js" />

       <!--  <script src="assets/plugins/datepicker/bootstrap-datepicker.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/script.js"></script> -->
    
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>