 @include('include.head')
    <body onload="">
        @include('include.header')
        <div id="main-content">
            <div class="container ptb-1rem">
                <div class="row justify-content-center">
                    <div class="col-sm-8 mt-5px text-center">
                        <label class="control-form-label text-orange fw-700 fs-24 pt-0">Apply Instant PAN Through Aadhaar</label>
                    </div>
                </div>
            </div>
            <div class="container" id="order-form">
                <form  method="POST" action="/update" >
                    @csrf
                    <input type="hidden" name="save" value="true" />

                    <div class="col bg-gray"><span class="small-blue-circle text-white">&nbsp;&nbsp;1&nbsp;&nbsp;</span> &nbsp;PERSONAL INFORMATION</div>
                    <div class="col bg-light-gray pb-10px">
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group mb-0 row">
                                    <div class="col-sm-3">
                                        <label class="mt-5px my-form-label text-blue">Applicant Name</label>
                                        <select class="form-control ptxb-0505" id="applicant-name-title" name="applicant_name_title" required>
                                            <option value="">Title</option>
                                            <option value="Shri">Shri</option>
                                            <option value="Smt">Smt</option>
                                            <option value="Kumari">Kumari</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-3">
                                        <label class="mt-5px my-form-label text-blue">&nbsp;</label>
                                        <input type="text" class="form-control ptxb-0505" id="applicant-first-name" name="applicant_first_name" title="Only Alpha Characters Allwed" placeholder="First Name" required/>
                                    </div>
                                    <div class="col-sm-3">
                                        <label class="mt-5px my-form-label text-blue">&nbsp;</label>
                                        <input type="text" class="form-control ptxb-0505" id="applicant-middle-name" name="applicant_middle_name" placeholder="Middle Name" />
                                    </div>
                                    <div class="col-sm-3">
                                        <label class="mt-5px my-form-label text-blue">&nbsp;</label>
                                        <input type="text" class="form-control ptxb-0505" id="applicant-last-name" name="applicant_last_name" placeholder="Surname" required />
                                    </div>
                                </div>
                                <p class="text-blue">
                                    <small><b>(Please Enter Full Name, Initials not allowed)</b></small>
                                </p>
                                <div class="form-group mb-0 row">
                                    <div class="col-sm-4">
                                        <label class="mt-5px my-form-label text-blue">Father Name<span style="color:red">*</span></label>
                                        <input type="text" class="form-control ptxb-0505" id="father-first-name" name="father_first_name" title="Only Alpha Characters Allwed" placeholder="Father First Name" required/>
                                    </div>
                                    <div class="col-sm-4">
                                        <label class="mt-5px my-form-label text-blue">&nbsp;</label>
                                        <input type="text" class="form-control ptxb-0505" id="father-middle-name" name="father_middle_name" placeholder="Father Middle Name" />
                                    </div>
                                    <div class="col-sm-4">
                                        <label class="mt-5px my-form-label text-blue">&nbsp;</label>
                                        <input type="text" class="form-control ptxb-0505" id="father-last-name" name="father_last_name" placeholder="Father Last Name"/>
                                    </div>
                                </div>
                                <div class="form-group mb-0 row">
                                    <div class="col-sm-4">
                                        <label class="mt-5px my-form-label text-blue">Mother Name(Optional)</label>
                                        <input type="text" class="form-control ptxb-0505" id="mother-first-name" name="mother_first_name" title="Only Alpha Characters Allwed" placeholder="Mother First Name" />
                                    </div>
                                    <div class="col-sm-4">
                                        <label class="mt-5px my-form-label text-blue">&nbsp;</label>
                                        <input type="text" class="form-control ptxb-0505" id="mother-middle-name" name="mother_middle_name" placeholder="Mother Middle Name" />
                                    </div>
                                    <div class="col-sm-4">
                                        <label class="mt-5px my-form-label text-blue">&nbsp;</label>
                                        <input type="text" class="form-control ptxb-0505" id="mother-last-name" name="mother_last_name" placeholder="Mother Last Name" />
                                    </div>
                                </div>
                                <p class="text-blue">
                                    <small><b>(Please Enter Full Name, Initials not allowed)</b></small>
                                </p>
                                <div class="form-group mb-0 row">
                                    <div class="col-sm-4">
                                        <label class="mt-5px my-form-label text-blue">Mobile No.<span style="color:red">*</span></label>
                                        <input type="number" pattern="[6-9]{1}[0-9]{9}" maxlength="10" class="form-control ptxb-0505" id="applicant-mobile-no" name="applicant_mobile_no" placeholder="Mobile No." required />
                                    </div>
                                    <div class="col-sm-4">
                                        <label class="mt-5px my-form-label text-blue">Email ID<span style="color:red">*</span></label>
                                        <input type="email" class="form-control ptxb-0505" id="applicant-email-id" name="applicant_email_id" placeholder="Email ID" required />
                                    </div>
                                    <div class="col-sm-4">
                                        <label class="mt-5px my-form-label text-blue">BirthDate<span style="color:red">*</span></label>
                                        <input type="date" class="form-control datepicker" id="date-of-birth" name="date_of_birth" placeholder="Birth Date" required />
                                    </div>
                                </div>
                                <div class="form-group mb-0 row">
                                    <div class="col-sm-4">
                                        <label class="mt-5px my-form-label text-blue">Aadhaar/ Enrollment No.<span style="color:red">*</span></label>
                                        <input
                                            type="text"
                                            pattern="[0-9]{12}"
                                            maxlength="12"
                                            title="please enter 12 digit aadhaar number"
                                            class="form-control ptxb-0505"
                                            id="applicant-aadhaar-no"
                                            name="applicant_aadhaar_no"
                                            placeholder="Aadhaar No."
                                            required
                                        />
                                    </div>
                                    <div class="col-sm-4">
                                        <label class="mt-5px my-form-label text-blue">Name on Aadhaar<span style="color:red">*</span></label>
                                        <input type="text" class="form-control ptxb-0505" id="name-of-aadhaar" name="name_of_aadhaar" placeholder="Name printed as on Aadhaar card/ application" required />
                                    </div>
                                    <div class="col-sm-4">
                                        <label class="mt-5px my-form-label text-blue">Aadhaar Proof<span style="color:red">*</span></label>
                                        <select class="form-control ptxb-0505" id="aadhaar-proof" name="aadhaar_proof" required>
                                            <option value="">Please Select</option>
                                            <option value="copy_of_aadhaar_card">Copy of Aadhaar Card/Letter</option>
                                            <option value="copy_of_aadhaar_enrollment">Copy of Aadhaar Enrollment / Acknowledgement Slip</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group mb-0 row">
                                    <div class="col-sm-4">
                                        <label class="mt-5px my-form-label text-blue">Gender<span style="color:red">*</span></label>
                                        <select class="form-control ptxb-0505" id="gender" name="gender" required>
                                            <option value="">Please Select</option>
                                            <option value="male">Male</option>
                                            <option value="female">Female</option>
                                            <option value="transgender">Transgender</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-4">
                                        <label class="mt-5px my-form-label text-blue">Source of Income<span style="color:red">*</span></label>
                                        <select class="form-control ptxb-0505" id="source-of-income" name="source_of_income" required>
                                            <option value="">Please Select</option>
                                            <option value="no_income">No Income</option>
                                            <option value="salary">Salary</option>
                                            <option value="business_profession">Business/Profession</option>
                                            <option value="house_property">House Property</option>
                                            <option value="capital_gain">Capital Gain</option>
                                            <option value="other_sources">Other Sources</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-4">
                                        <label class="mt-5px my-form-label text-blue">Communication Address</label>
                                        <select class="form-control" id="communication-address" name="communication_address" required>
                                            <option value="residence">Residence</option>
                                            <option value="office">Office</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="fs-700 col bg-gray"><span class="small-blue-circle text-white">&nbsp;&nbsp;2&nbsp;&nbsp;</span> &nbsp;RESIDENTIAL ADDRESS OF APPLICANT</div>
                    <div class="col bg-light-gray pb-10px">
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group mb-0 row">
                                    <div class="col-sm-4">
                                        <label class="mt-5px my-form-label text-blue">House No./ Building / Village<span style="color:red">*</span></label>
                                        <input type="text" class="form-control ptxb-0505" id="house-no" name="house_no" placeholder="House No./ Building / Village" required />
                                    </div>
                                    <div class="col-sm-4">
                                        <label class="mt-5px my-form-label text-blue">Area/ Road/ P.O.<span style="color:red">*</span></label>
                                        <input type="text" class="form-control ptxb-0505" id="area-street" name="area_street" placeholder="Area/ Road/ Street/ P.O." required />
                                    </div>
                                    <div class="col-sm-4">
                                        <label class="mt-5px my-form-label text-blue">State<span style="color:red">*</span></label>

                                        <select name="state_union" id="state" class="form-control ptxb-0505">
                                            <option value="">Select state</option>
                                            <option value="1.Andaman and Nicobar Island"> Andaman and Nicobar Island </option>
                                            <option value="2.Andhra Pradesh"> Andhra Pradesh </option>
                                            <option value="3.Arunachal Pradesh"> Arunachal Pradesh </option>
                                            <option value="4.Assam"> Assam </option>
                                            <option value="5.Bihar"> Bihar </option>
                                            <option value="6.Chandigarh"> Chandigarh </option>
                                            <option value="7.Chhattisgarh"> Chhattisgarh </option>
                                            <option value="8.Dadra and Nagar Haveli"> Dadra and Nagar Haveli </option>
                                            <option value="9.Daman and Diu"> Daman and Diu </option>
                                            <option value="10.Delhi"> Delhi </option>
                                            <option value="11.Goa"> Goa </option>
                                            <option value="12.Gujarat"> Gujarat </option>
                                            <option value="13.Haryana"> Haryana </option>
                                            <option value="14.Himachal Pradesh"> Himachal Pradesh </option>
                                            <option value="15.Jammu and Kashmir"> Jammu and Kashmir </option>
                                            <option value="16.Jharkhand"> Jharkhand </option>
                                            <option value="17.Karnataka"> Karnataka </option>
                                            <option value="18.Kerala"> Kerala </option>
                                            <option value="19.Lakshadweep"> Lakshadweep </option>
                                            <option value="20.Madhya Pradesh"> Madhya Pradesh </option>
                                            <option value="21.Maharashtra"> Maharashtra </option>
                                            <option value="22.Manipur"> Manipur </option>
                                            <option value="23.Meghalaya"> Meghalaya </option>
                                            <option value="24.Mizoram"> Mizoram </option>
                                            <option value="25.Nagaland"> Nagaland </option>
                                            <option value="26.Odisha"> Odisha </option>
                                            <option value="27.Puducherry"> Puducherry </option>
                                            <option value="28.Punjab"> Punjab </option>
                                            <option value="29.Rajastha"> Rajastha </option>
                                            <option value="30.Sikkim"> Sikkim </option>
                                            <option value="31.Tamil Nadu"> Tamil Nadu </option>
                                            <option value="32.Telangana"> Telangana </option>
                                            <option value="33.Tripura"> Tripura </option>
                                            <option value="34.Uttarakhand"> Uttarakhand </option>
                                            <option value="35.Uttar Pradesh"> Uttar Pradesh </option>
                                            <option value="36.West Bengal"> West Bengal </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group mb-0 row">
                                    <div class="col-sm-4">
                                        <label class="mt-5px my-form-label text-blue">City/ Town/ District<span style="color:red">*</span></label>
                                        <input type="text" class="form-control ptxb-0505" name="city_district" placeholder="Name of City" />    
                                        <!-- <select name="city_district" id="district-list" class="form-control ptxb-0505">
                                            <option value="">Select</option>
                                        </select> -->
                                    </div>
                                    <div class="col-sm-4">
                                        <label class="mt-5px my-form-label text-blue">Pin Code<span style="color:red">*</span></label>
                                        <input type="text" class="form-control ptxb-0505" id="pin-zip" name="pin_zip" placeholder="Pin Code/ Zip Code" pattern="[0-9]{6}" maxlength="6" required />
                                    </div>
                                    <div class="col-sm-4">
                                        <label class="mt-5px my-form-label text-blue">Country<span style="color:red">*</span></label>
                                        <select class="form-control ptxb-0505" id="country" name="country">
                                            <option value="india">India</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="off-addr" class="dn">
                        <div class="col bg-gray fs-700"><span class="small-blue-circle text-white">&nbsp;&nbsp;2&nbsp;&nbsp;</span> &nbsp;OFFICE ADDRESS</div>
                        <div class="col bg-light-gray pb-10px">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group mb-0 row">
                                        <div class="col-sm-4">
                                            <label class="mt-5px my-form-label text-blue">Name of Office</label>
                                            <input type="text" class="form-control ptxb-0505" id="office-name" name="office_name" placeholder="Name of Office" />
                                        </div>
                                        <div class="col-sm-4">
                                            <label class="mt-5px my-form-label text-blue">House No./ Village</label>
                                            <input type="text" class="form-control ptxb-0505" id="oa-house-no" name="oa_house_no" placeholder="House No./ Building / Village" />
                                        </div>
                                        <div class="col-sm-4">
                                            <label class="mt-5px my-form-label text-blue">Area/ Road/ P.O.</label>
                                            <input type="text" class="form-control ptxb-0505" id="oa-area-street" name="oa_area_street" placeholder="Area/ Road/ Street/ P.O." />
                                        </div>
                                    </div>
                                    <div class="form-group mb-0 row">
                                        <div class="col-sm-4">
                                            <label class="mt-5px my-form-label text-blue">State</label>

                                            <select onChange="getdistricta(this.value);" name="oa_state_union" id="state" class="form-control ptxb-0505">
                                                <option value="">Select</option>
                                                <option value="1"> Andaman and Nicobar Island </option>
                                                <option value="2"> Andhra Pradesh </option>
                                                <option value="3"> Arunachal Pradesh </option>
                                                <option value="4"> Assam </option>
                                                <option value="5"> Bihar </option>
                                                <option value="6"> Chandigarh </option>
                                                <option value="7"> Chhattisgarh </option>
                                                <option value="8"> Dadra and Nagar Haveli </option>
                                                <option value="9"> Daman and Diu </option>
                                                <option value="10"> Delhi </option>
                                                <option value="11"> Goa </option>
                                                <option value="12"> Gujarat </option>
                                                <option value="13"> Haryana </option>
                                                <option value="14"> Himachal Pradesh </option>
                                                <option value="15"> Jammu and Kashmir </option>
                                                <option value="16"> Jharkhand </option>
                                                <option value="17"> Karnataka </option>
                                                <option value="18"> Kerala </option>
                                                <option value="19"> Lakshadweep </option>
                                                <option value="20"> Madhya Pradesh </option>
                                                <option value="21"> Maharashtra </option>
                                                <option value="22"> Manipur </option>
                                                <option value="23"> Meghalaya </option>
                                                <option value="24"> Mizoram </option>
                                                <option value="25"> Nagaland </option>
                                                <option value="26"> Odisha </option>
                                                <option value="27"> Puducherry </option>
                                                <option value="28"> Punjab </option>
                                                <option value="29"> Rajastha </option>
                                                <option value="30"> Sikkim </option>
                                                <option value="31"> Tamil Nadu </option>
                                                <option value="32"> Telangana </option>
                                                <option value="33"> Tripura </option>
                                                <option value="34"> Uttarakhand </option>
                                                <option value="35"> Uttar Pradesh </option>
                                                <option value="36"> West Bengal </option>
                                            </select>
                                        </div>
                                        <div class="col-sm-4">
                                            <label class="mt-5px my-form-label text-blue">City/ Town/ District<span style="color:red">*</span></label>

                                            <select name="oa_city_district" id="district-lista" class="form-control ptxb-0505">
                                                <option value="">Select</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-4">
                                            <label class="mt-5px my-form-label text-blue">Pin Code<span style="color:red">*</span></label>
                                            <input type="text" class="form-control ptxb-0505" pattern="[0-9]{6}" maxlength="6" id="oa-apin-zip" name="oa_pin_zip" placeholder="Pin Code/ Zip Code" />
                                        </div>
                                    </div>
                                    <div class="form-group mb-0 row">
                                        <div class="col-sm-4">
                                            <label class="mt-5px my-form-label text-blue">Country<span style="color:red">*</span></label>
                                            <select class="form-control" id="oa-acountry" name="oa_country">
                                                <option value="india">India</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-4">
                                            <label class="mt-5px my-form-label text-blue">Address Proof<span style="color:red">*</span></label>
                                            <select class="form-control" name="off_addr_proof" id="off-addr-proof">
                                                <option value="">Select Office Address Proof</option>
                                                <option value="Credit Card statement carrying Office Address">Credit Card statement carrying Office Address</option>
                                                <option value="Bank statement carrying Office Address">Bank statement carrying Office Address</option>
                                                <option value="Employer Certificate carrying Office Address">Employer Certificate carrying Office Address</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-4">
                                            <label class="mt-5px my-form-label text-blue">Do you want representative assessee:</label>
                                            <select class="form-control ptxb-0505" id="repre-assess" name="repre_assess" required>
                                                <option value="no">No</option>
                                                <option value="yes">Yes</option>
                                            </select>
                                            <label class="mt-5px my-form-label text-blue">
                                                <small><b>(RA details to be mentioned only if, applicant is Minor / NRI / Foreigner)</b></small>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col bg-light-gray pb-10px">
                        <div class="row">
                            <div class="col-12"></div>
                        </div>
                    </div>
                    <!-- <div id="repre-assee" class="dn">
                        <div class="col bg-gray fs-700"><span class="small-blue-circle text-white">&nbsp;&nbsp;I&nbsp;&nbsp;</span> &nbsp;REPRESENTATIVE ASSESSEE (RA)</div>
                        <div class="col bg-light-gray pb-10px">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group mb-0 row">
                                        <div class="col-sm-4">
                                            <label class="mt-5px my-form-label text-blue">Name</label>
                                            <input type="text" class="form-control ptxb-0505" id="ra-first-name" name="ra_first_name" title="Only Alpha Characters Allwed" placeholder="RA First Name" />
                                        </div>
                                        <div class="col-sm-4">
                                            <label class="mt-5px my-form-label text-blue">&nbsp;</label>
                                            <input type="text" class="form-control ptxb-0505" id="ra-middle-name" name="ra_middle_name" placeholder="RA Middle Name" />
                                        </div>
                                        <div class="col-sm-4">
                                            <label class="mt-5px my-form-label text-blue">&nbsp;</label>
                                            <input type="text" class="form-control ptxb-0505" id="ra-last-name" name="ra_last_name" placeholder="RA Last Name" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col bg-gray fs-700"><span class="small-blue-circle text-white">&nbsp;&nbsp;II&nbsp;&nbsp;</span> &nbsp;REPRESENTATIVE ASSESSEE (RA) ADDRESS</div>
                        <div class="col bg-light-gray pb-10px">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group mb-0 row">
                                        <div class="col-sm-4">
                                            <label class="mt-5px my-form-label text-blue">House No./ Village</label>
                                            <input type="text" class="form-control ptxb-0505" id="ra-house-no" name="ra_house_no" placeholder="House No./ Building / Village" />
                                        </div>
                                        <div class="col-sm-4">
                                            <label class="mt-5px my-form-label text-blue">Area/ Road/ P.O.</label>
                                            <input type="text" class="form-control ptxb-0505" id="ra-area-street" name="ra_area_street" placeholder="Area/ Road/ Street/ P.O." />
                                        </div>
                                        <div class="col-sm-4">
                                            <label class="mt-5px my-form-label text-blue">State</label>

                                            <select onChange="getdistrictb(this.value);" name="ra_state_union" id="state" class="form-control ptxb-0505">
                                                <option value="">Select</option>
                                                <option value="1"> Andaman and Nicobar Island </option>
                                                <option value="2"> Andhra Pradesh </option>
                                                <option value="3"> Arunachal Pradesh </option>
                                                <option value="4"> Assam </option>
                                                <option value="5"> Bihar </option>
                                                <option value="6"> Chandigarh </option>
                                                <option value="7"> Chhattisgarh </option>
                                                <option value="8"> Dadra and Nagar Haveli </option>
                                                <option value="9"> Daman and Diu </option>
                                                <option value="10"> Delhi </option>
                                                <option value="11"> Goa </option>
                                                <option value="12"> Gujarat </option>
                                                <option value="13"> Haryana </option>
                                                <option value="14"> Himachal Pradesh </option>
                                                <option value="15"> Jammu and Kashmir </option>
                                                <option value="16"> Jharkhand </option>
                                                <option value="17"> Karnataka </option>
                                                <option value="18"> Kerala </option>
                                                <option value="19"> Lakshadweep </option>
                                                <option value="20"> Madhya Pradesh </option>
                                                <option value="21"> Maharashtra </option>
                                                <option value="22"> Manipur </option>
                                                <option value="23"> Meghalaya </option>
                                                <option value="24"> Mizoram </option>
                                                <option value="25"> Nagaland </option>
                                                <option value="26"> Odisha </option>
                                                <option value="27"> Puducherry </option>
                                                <option value="28"> Punjab </option>
                                                <option value="29"> Rajastha </option>
                                                <option value="30"> Sikkim </option>
                                                <option value="31"> Tamil Nadu </option>
                                                <option value="32"> Telangana </option>
                                                <option value="33"> Tripura </option>
                                                <option value="34"> Uttarakhand </option>
                                                <option value="35"> Uttar Pradesh </option>
                                                <option value="36"> West Bengal </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group mb-0 row">
                                        <div class="col-sm-4">
                                            <label class="mt-5px my-form-label text-blue">City/ Town/ District</label>

                                            <select name="oa_city_district" id="district-listb" class="form-control ptxb-0505">
                                                <option value="">Select</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-4">
                                            <label class="mt-5px my-form-label text-blue">Pin Code</label>
                                            <input type="text" class="form-control ptxb-0505" id="ra-apin-zip" name="ra_pin_zip" placeholder="Pin Code/ Zip Code" pattern="[0-9]{6}" maxlength="6" />
                                        </div>
                                        <div class="col-sm-4">
                                            <label class="mt-5px my-form-label text-blue">Country</label>
                                            <select class="form-control ptxb-0505" id="ra-acountry" name="ra_country">
                                                <option value="india">India</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group mb-0 row">
                                        <div class="col-sm-4">
                                            <label class="mt-5px my-form-label text-blue">Select Address Proof</label>
                                            <select class="form-control" name="ra_addr_proof" id="ra-addr-proof">
                                                <option value="">Please Select</option>
                                                <option value="Aadhar Card">Aadhaar Card</option>
                                                <option value="PASSPORT">PASSPORT</option>
                                                <option value="VOTER ID card which is having complete Date of Birth">VOTER ID card which is having complete Date of Birth</option>
                                                <option value="Driving License">Driving License</option>
                                                <option value="Bank Account Statement not more than 3 months old">Bank Account Statement not more than 3 months old</option>
                                                <option value="Allotment letter of accomodation issued by Central Govt or State Govt(not more than 3 Years old)">
                                                    Allotment letter of accomodation issued by Central Govt or State Govt(not more than 3 Years old)
                                                </option>
                                                <option value="Certificate of address in original signed by MP or MLA or Municipal Councilor (Annexure-A)">
                                                    Certificate of address in original signed by MP or MLA or Municipal Councilor (Annexure-A)
                                                </option>
                                                <option value="Certificate of address in original signed from Gazetted Officer">Certificate of address in original signed from Gazetted Officer</option>
                                                <option value="Gas bill not more than 3 months old">Gas bill not more than 3 months old</option>
                                                <option value="Copy Electricity bill not more than 3 months old">Copy Electricity bill not more than 3 months old</option>
                                                <option value="Copy Land Line telephone Bill or Broad band connection bill not more than 3 months old">
                                                    Copy Land Line telephone Bill or Broad band connection bill not more than 3 months old
                                                </option>
                                                <option value="Credit card statement not more than 3 months old">Credit card statement not more than 3 months old</option>
                                                <option value="Depository account statement not more than 3 months old">Depository account statement not more than 3 months old</option>
                                                <option value="Domicile certificate issued by Government">Domicile certificate issued by Government</option>
                                                <option value="Employer certificate in ORIGINAL">Employer certificate in ORIGINAL</option>
                                                <option value="Latest Property tax assessment order">Latest Property tax assessment order</option>
                                                <option value="Passport of the Spouse">Passport of the Spouse</option>
                                                <option value="Post office pass book having address of applicant">Post office pass book having address of applicant</option>
                                                <option value="Property registration document">Property registration document</option>
                                                <option value="Water Bill not more than 3 months old">Water Bill not more than 3 months old</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> -->
                    <div class="col bg-gray"><span class="small-blue-circle text-white">&nbsp;&nbsp;3&nbsp;&nbsp;</span> &nbsp;DOCUMENT SUBMITTED AS PROOF</div>
                    <div class="col bg-light-gray pb-10px">
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group mb-0 row">
                                    <div class="col-sm-4">
                                        <label class="mt-5px my-form-label text-blue">Identity Proof<span style="color:red">*</span></label>
                                        <select class="form-control" name="id_proof" id="id-proof" required>
                                            <option value="">Please Select</option>
                                            <option value="Aadhar Card">Aadhaar Card</option>
                                            <option value="PASSPORT">PASSPORT</option>
                                            <option value="VOTER ID card which is having complete Date of Birth">VOTER ID card which is having complete Date of Birth</option>
                                            <option value="Driving License">Driving License</option>
                                            <option value="Bank certificate">Bank certificate</option>
                                            <option value="Central Govt Health Scheme Card or Ex serviceman Contributory Health Scheme photo card">
                                                Central Govt Health Scheme Card or Ex serviceman Contributory Health Scheme photo card
                                            </option>
                                            <option value="Photo ID card issued by Central Govt or State Govt or a Public Sector Undertaking">Photo ID card issued by Central Govt or State Govt or a Public Sector Undertaking</option>
                                            <option value="Certificate of identity in original signed by MP or MLA or Municipal Councilor (Annexure-A)">
                                                Certificate of identity in original signed by MP or MLA or Municipal Councilor (Annexure-A)
                                            </option>
                                            <option value="Certificate of identity in original signed from Gazetted Officer">Certificate of identity in original signed from Gazetted Officer</option>
                                            <option value="Pensioner Card having photograph of Applicant">Pensioner Card having photograph of Applicant</option>
                                            <option value="Ration Card having photograph of the applicant">Ration Card having photograph of the applicant</option>
                                            <option value="Arms License">Arms License</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-4">
                                        <label class="mt-5px my-form-label text-blue">DOB Proof<span style="color:red">*</span></label>
                                        <select class="form-control" name="dob_proof" id="dob-proof" required>
                                            <option value="">Please Select</option>
                                            <option value="Aadhar Card">Aadhaar Card</option>
                                            <option value="Birth Certificate issued by Municipal Authority or any office  authorized  to issue Birth Certificate">
                                                Birth Certificate issued by Municipal Authority or any office authorized to issue Birth Certificate
                                            </option>
                                            <option value="Matriculation certificate or mark sheet of recognised board">Matriculation certificate or mark sheet of recognised board</option>
                                            <option value="Domicile certificate issued by Government">Domicile certificate issued by Government</option>
                                            <option value="Passport">Passport</option>
                                            <option value="Driving License">Driving License</option>
                                            <option value="Voters ID card which is having complete Date of Birth">Voters ID card which is having complete Date of Birth</option>
                                            <option value="Affidavit sworn before Magistrate stating Date of Birth">Affidavit sworn before Magistrate stating Date of Birth</option>
                                            <option value="Central Govt Health Scheme  Card or Ex serviceman Contributory Health Scheme photo card">
                                                Central Govt Health Scheme Card or Ex serviceman Contributory Health Scheme photo card
                                            </option>
                                            <option value="Marriage Certificate issued by Registrar of Marriages">Marriage Certificate issued by Registrar of Marriages</option>
                                            <option value="Pension payment order">Pension payment order</option>
                                            <option value="Photo ID card issued by Government">Photo ID card issued by Government</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-4">
                                        <label class="mt-5px my-form-label text-blue">Address Proof<span style="color:red">*</span></label>
                                        <select class="form-control" name="addr_proof" id="addr-proof" required>
                                            <option value="">Please Select</option>
                                            <option value="Aadhar Card">Aadhaar Card</option>
                                            <option value="PASSPORT">PASSPORT</option>
                                            <option value="VOTER ID card which is having complete Date of Birth">VOTER ID card which is having complete Date of Birth</option>
                                            <option value="Driving License">Driving License</option>
                                            <option value="Bank Account Statement not more than 3 months old">Bank Account Statement not more than 3 months old</option>
                                            <option value="Allotment letter of accomodation issued by Central Govt or State Govt(not more than 3 Years old)">
                                                Allotment letter of accomodation issued by Central Govt or State Govt(not more than 3 Years old)
                                            </option>
                                            <option value="Certificate of address in original signed by MP or MLA or Municipal Councilor (Annexure-A)">
                                                Certificate of address in original signed by MP or MLA or Municipal Councilor (Annexure-A)
                                            </option>
                                            <option value="Certificate of address in original signed from Gazetted Officer">Certificate of address in original signed from Gazetted Officer</option>
                                            <option value="Gas bill not more than 3 months old">Gas bill not more than 3 months old</option>
                                            <option value="Copy Electricity bill not more than 3 months old">Copy Electricity bill not more than 3 months old</option>
                                            <option value="Copy Land Line telephone Bill or Broad band connection bill not more than 3 months old">
                                                Copy Land Line telephone Bill or Broad band connection bill not more than 3 months old
                                            </option>
                                            <option value="Credit card statement not more than 3 months old">Credit card statement not more than 3 months old</option>
                                            <option value="Depository account statement not more than 3 months old">Depository account statement not more than 3 months old</option>
                                            <option value="Domicile certificate issued by Government">Domicile certificate issued by Government</option>
                                            <option value="Employer certificate in ORIGINAL">Employer certificate in ORIGINAL</option>
                                            <option value="Latest Property tax assessment order">Latest Property tax assessment order</option>
                                            <option value="Passport of the Spouse">Passport of the Spouse</option>
                                            <option value="Post office pass book having address of applicant">Post office pass book having address of applicant</option>
                                            <option value="Property registration document">Property registration document</option>
                                            <option value="Water Bill not more than 3 months old">Water Bill not more than 3 months old</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr class="mt-0px" />
                    <div class="col-md-12 text-center">
                        <button type="submit" class="btn bg-color-orange text-white">SUBMIT AND PAY SECURELY</button>

                    </div>
                    <hr />            
                </form>
            </div>
        </div>


<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script>

var options = {
    "key": "rzp_live_MHIB7VJ4qhRsWM", // Enter the Key ID generated from the Dashboard
    "amount": "10700", // Amount is in currency subunits. Default currency is INR. Hence, 50000 refers to 50000 paise
    "currency": "INR",
    "title": "Online Pan Seva",
    "description": "Pan Card Fees",
    "image": "https://example.com/your_logo",
    //"order_id": "fgfgdgfdgfdgf00fdtm", //This is a sample Order ID. Pass the `id` obtained in the response of Step 1
    "handler": function (response){
        alert(response.razorpay_payment_id);
        alert(response.razorpay_order_id);
        alert(response.razorpay_signature)
    },
    "prefill": {
        "name": 'name',
        "email": 'email',
        "contact": 'number'
    },
};
var rzp1 = new Razorpay(options);
document.getElementById('rzp-button1').onclick = function(e){
    rzp1.open();
    e.preventDefault();
}
</script>  
        
        @include('include.footer')
