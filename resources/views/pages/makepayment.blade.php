 @include('include.head')
    <body onload="">
        <div class="header" id="header">
            <div class="container-fluid mb-10px">
                <div class="row text-center">
                    <div class="col">
                        <a href="/"><img class="header-left-image" src="{{ asset('assets/img/online-msme-logo.png') }}" /></a>
                    </div>
                    <div class="col">
                        <img class="header-center-image mt-20px" src="{{ asset('assets/img/online-msme-center-image.png') }}" />
                    </div>
                    <div class="col d-none d-md-block">
                        <img class="header-right-image" src="{{ asset('assets/img/shopact-msme-gst-swacha-bharat.png') }}" />
                    </div>
                </div>
            </div>
            <nav class="navbar navbar-light bg-light-gray">
               
                <div class="navbar-collapse justify-content-md-center collapse" style="">
                    <ul class="navbar-nav">
                    
                    </ul>
                </div>
            </nav>
        </div>
        <div id="main-content">
            <div class="container ptb-1rem">
                <div class="row justify-content-center">
                    <div class="col-sm-8 mt-5px text-center">
                        <label class="control-form-label text-orange fw-700 fs-24 pt-0"></label>
                    </div>
                </div>
            </div>
            @if (Session::has('message'))
               <div class="alert alert-success" role="alert">
                   {{Session::get('message')}}
               </div>
            @endif
            <div class="container">
                <div class="">
                    <div class="col-md-12 text-center">
                        <label class="control-form-label text-orange fw-700 fs-24 pt-0">Application No: {{ $info->pan_id }}</label>
                    </div>
                    <div class="col-md-12 col-sm-12">
                        <div class="table-responsive">
                            <table class="table table-bordered" style="text-align: start;">
                                <tbody>
                                     
                                <tr>
                                  <td><b>Applicant Name:-</b> {{ $info->applicant_first_name }} {{ $info->applicant_middle_name }} {{ $info->applicant_last_name }}</td>
                                  
                                </tr>
                               
                                <tr>
                                  <td><b>Email ID:-</b>  {{ $info->applicant_email_id }}</td>
                                 
                                </tr>
                               
                                <tr>
                                  <td><b>Apply Date:-</b> {{ $info->timestamp }}</td>
                                  
                                </tr>
                                
                              </tbody>
                            </table>
                        </div>
                              
                            <div class="" style="text-align: center; margin-top: 20px; margin-bottom:20px;">
                                <button class="btn btn-primary" id="pay">Pay Securely</button>
                               
                            </div>
                            <!-- <div class="" style="text-align: center; margin-top: 20px; margin-bottom:20px;">
                                
                                <a href="https://paytm.me/Fl-heKB"><button class="btn btn-primary">Pay With Paytm</button></a>
                            </div> -->
                            
                            <div class="row">
                                <div class="col-md-4 col-xs-12">
                                    <div class="agree-img" class="img-responsive" >
                                        <img src="https://pass-india.org/img/payment-logos.png" class="img-responsive" style="width: 324px;">
                                    </div><!-- <br>
                                    <H5>Benefits of Having a Pan Application </H5>
                                    <p>- Identity Proof </p>
                                    <p>- Opening a Bank Account</p>
                                    <p>- IT Returns Filing</p>
                                    <p>- Starting a Business</p>
                                    <p>- Tax Deductions</p>
                                    <p>- Time Deposit</p>
                                    <p>- Purchase or Sale of Goods and Services</p>
                                    <p>- Foreign Travel</p>
                                </div> -->
                                
                            </div>
                            
                    </div>
                    <br>
                    <div class="col-md-12 text-center">
                        <label class="control-form-label text-orange fw-700 fs-24 pt-0">Contact US</label>
                        <hr>
                        <br>
                        <p>Email ID: <b><a class="text-orange td-none" href="">connect.panseva@gmail.com</span></a></b>                  
                        </p>
                        <p>Mo. Number: <b><a class="text-orange td-none" href="">+91 7435 803 802</span></a></b>                    
                        </p> 
                        <p>We are complying with the government regulations due to Covid-19. Our support teams are overwhelmed due to the high volumes of Application our executive contact you within 48Hour </p>               
                        


                        
                    </div>
                </div>    
            </div>
        </div>

<script type="application/javascript" crossorigin="anonymous" src="https://securegw.paytm.in/merchantpgpui/checkoutjs/merchants/vknUlk68127034744655.js" 
                onload="onScriptLoad();"></script>
        <script>
            function onScriptLoad(){
                var config = {
                 "root": "",
                 "flow": "DEFAULT",
                 "data": {
                  "orderId": "" /* update order id */,
                  "token": "" /* update token value */,
                  "tokenType": "TXN_TOKEN",
                  "amount": "" /* update amount */
                 },
                 "handler": {
                    "notifyMerchant": function(eventName,data){
                      console.log("notifyMerchant handler function called");
                      console.log("eventName => ",eventName);
                      console.log("data => ",data);
                    } 
                  }
                };

                if(window.Paytm && window.Paytm.CheckoutJS){
                    window.Paytm.CheckoutJS.onLoad(function excecuteAfterCompleteLoad() {
                        // initialze configuration using init method 
                        window.Paytm.CheckoutJS.init(config).then(function onSuccess() {
                           // after successfully update configuration invoke checkoutjs
                           window.Paytm.CheckoutJS.invoke();
                        }).catch(function onError(error){
                            console.log("error => ",error);
                        });
                    });
                } 
            }
            var rzp1 = (onScriptLoad);
            document.getElementById('paytm').onclick = function(e){
                rzp1.open();
                e.preventDefault();
            }
        </script>

<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script>

    
 var name = <?php echo json_encode( $result->first_name) ?>;
 var email = <?php echo json_encode( $result->email) ?>;
 var contact = <?php echo json_encode( $result->mobile_number) ?>;
 var order_id = <?php echo json_encode( $result->order_id) ?>;

var options = {
    "key": "rzp_live_MHIB7VJ4qhRsWM", //rzp_test_7fj4d4ByPL0wzy Enter the Key ID generated from the Dashboard
    "amount": "10700", // Amount is in currency subunits. Default currency is INR. Hence, 50000 refers to 50000 paise
    "currency": "INR",
    "title": "Online Pan Seva",
    "description": "Pan Card Fees",
    "image": "https://www.onlinepanseva.in/assets/img/favicon.ico",
    "order_id": order_id, //This is a sample Order ID. Pass the `id` obtained in the response of Step 1
    "callback_url": "https://www.onlinepanseva.in/updatetransaction",
    /*"handler": function (response){
            
            alert(response.razorpay_payment_id);
            alert(response.razorpay_order_id);
            alert(response.razorpay_signature)
            alert('Your Payment successfully');
            
            //window.location.href = "http://onlinepanseva.in";
    },*/
    "prefill": {
        "name": 'name',
        "email": email,
        "contact": contact
    },
};
var rzp1 = new Razorpay(options);
document.getElementById('pay').onclick = function(e){
    rzp1.open();
    e.preventDefault();
}

</script>  
        
        @include('include.footer')
