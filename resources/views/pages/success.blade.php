 @include('include.head')
    <body onload="">
        <div class="header" id="header">
            <div class="container-fluid mb-10px">
                <div class="row text-center">
                    <div class="col">
                        <a href="/"><img class="header-left-image" src="{{ asset('assets/img/online-msme-logo.png') }}" /></a>
                    </div>
                    <div class="col">
                        <img class="header-center-image mt-20px" src="{{ asset('assets/img/online-msme-center-image.png') }}" />
                    </div>
                    <div class="col d-none d-md-block">
                        <img class="header-right-image" src="{{ asset('assets/img/shopact-msme-gst-swacha-bharat.png') }}" />
                    </div>
                </div>
            </div>
            <nav class="navbar navbar-light bg-light-gray">
               
                <div class="navbar-collapse justify-content-md-center collapse" style="">
                    <ul class="navbar-nav">
                    
                    </ul>
                </div>
            </nav>
        </div>
        <div id="main-content">
            <div class="container ptb-1rem">
                <div class="row justify-content-center">
                    <div class="col-sm-8 mt-5px text-center">
                        <label class="control-form-label text-orange fw-700 fs-24 pt-0"></label>
                    </div>
                </div>
            </div>
            
            <div class="container">
                <div class="">
                    <div class="col-md-12 text-center">
                        <img class="header-left-image" src="{{ asset('assets/img/success.svg') }}" />
                        <h3>Rs.107.00</h3>
                        <label class="control-form-label text-orange fw-700 fs-24 pt-0">Paid Successfully</label>
                    </div>
                    <div class="col-md-12 col-sm-12">
                        <div class="table-responsive">
                            <table class="table table-bordered" style="text-align: start;">
                                <tbody>
                                <tr>
                                  <td><b>Application ID:-</b> {{ $data->pan_id }} </td>
                                  
                                </tr>     
                                <tr>
                                  <td><b>Payment ID:-</b> {{ $data->payment_id }} </td>
                                  
                                </tr>
                               
                                <tr>
                                  <td><b>Order ID:-</b> {{ $data->order_id }}  </td>
                                 
                                </tr>
                               
                                <tr>
                                  <td><b>Date:-</b> {{ $data->created_at }}  </td>
                                  
                                </tr>
                                
                              </tbody>
                            </table>
                        </div>
                              <p>We are complying with the government regulations due to Covid-19. Our support teams are overwhelmed due to the high volumes of Application our executive contact you within 48Hour </p>  
                            <div class="row">
                                <div class="col-md-4 col-xs-12">
                                    <div class="agree-img" class="img-responsive" >
                                        <img src="https://pass-india.org/img/payment-logos.png" class="img-responsive" style="width: 324px;">
                                    </div>
                                </div>
                            </div>
                            
                    </div>
                    <br>
                    <div class="col-md-12 text-center">
                        <label class="control-form-label text-orange fw-700 fs-24 pt-0">Contact US</label>
                        <hr>
                        <br>
                        <p>Email ID: <b><a class="text-orange td-none" href="">connect.panseva@gmail.com</span></a></b>                  
                        </p>
                        <p>Mo. Number: <b><a class="text-orange td-none" href="">+91 7435 803 802</span></a></b>                    
                        </p>                
                        <p class="text-orange">Support hours: Mon-Fri 10AM-6PM</p>
                    </div>
                </div>    
            </div>
        </div>


        
        @include('include.footer')
