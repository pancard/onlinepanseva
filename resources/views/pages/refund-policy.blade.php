@include('include.head')
<body>
	<div class="header" id="header">
		@include('include.head')
	</div>
	<div id="main-content">
		<div id="order-form-thank-you" class="container-fluid p-20px mt-20px">
			<div class="container">
				<div class="col-md-12 text-center text-blue bg-white p-40px box-shadow">
					<div class="col-md-12 text-center">
						<label class="control-form-label text-orange fw-700 fs-24 pt-0">REFUND POLICY</label>
						<hr>
						<br>
						<label class="control-form-label text-blue pt-0"><b>1. REFUND OF PAYMENT RECEIVED</b>
						</label>
						<hr class="hr">
						<p class="text-justify uppercase mt-20px">For the PAN Services that are given to you, https://onlinepanseva.in/, will only accept refunds if the service is not provided. https://onlinepanseva.in/, reserves the right to determine a fair value of the product on return and the same shall be binding on both parties. The refund process will be initiated once we confirmation of the services not provided. In case of refund request accepted, amount WILL BE REFUNDED IN THE SAME MODE YOU HAVE PAID. ALSO IF THERE IS DELAY BEYOND 45 DAYS FOR SUBMITTING HARD COPY OF FORM, NO REFUND SHALL BE MADE AGAINST SUCH TRANSACTION.</p>
						<br>
						<label class="control-form-label text-blue pt-0"><b>2. REFUND REQUEST</b>
						</label>
						<hr class="hr">
						<p class="text-justify uppercase mt-20px">REFUND REQUEST CAN BE SEND AT <a href="cdn-cgi/l/email-protection.html" class="__cf_email__" data-cfemail="20434f4e544143540e50414e494e4460474d41494c0e434f4d0e">[email&#160;protected]</a> REFUND REQUEST CAN BE MADE WITHIN 10 DAYS OF ONLINE APPLICATION MADE.</p>
						<br>
						<label class="control-form-label text-blue pt-0"><b>3. CANCELLATION OF APPLICATION</b>
						</label>
						<hr class="hr">
						<p class="text-justify uppercase mt-20px">YOU CANNOT CANCEL THE APPLICATION ONCE PROCESSED FROM OUR SIDE. NO REFUND WILL BE PROVIDED ONCE THE APPLICATION IS DONE.</p>
						<br>
						<label class="control-form-label text-blue pt-0"><b>4. ISSUANCE OF PAN CARD</b>
						</label>
						<hr class="hr">
						<p class="text-justify uppercase mt-20px">PANCARD APPLIED, WILL BE DELIVERED WITHIN 30 TO 45 DAYS (AFTER RECEIPT OF HARD COPY APPLICATION) OF AT THE ADDRESS PROVIDED IN THE APPLICATION FORM.</p>
						<br>
						<label class="control-form-label text-blue pt-0"><b>5. REFUND ANY CASE</b>
						</label>
						<hr class="hr">
						<p class="text-justify uppercase mt-20px">if in any case you want to get a refund after completing payment, then we will debited processing charges, GST charges, and payment gateway charges.</p>
						<br>
						<label class="control-form-label text-blue pt-0"><b>6. FORCE MAJEURE</b>
						</label>
						<hr class="hr">
						<p class="text-justify uppercase mt-20px">https://onlinepanseva.in/ SHALL NOT BE CONSIDERED IN BREACH OF ITS SATISFACTION GUARANTEE POLICY OR DEFAULT UNDER ANY TERMS OF SERVICE, AND SHALL NOT BE LIABLE TO THE CLIENT FOR ANY CESSATION, INTERRUPTION, OR DELAY IN THE PERFORMANCE OF ITS OBLIGATIONS BY REASON OF EARTHQUAKE, FLOOD, FIRE, STORM, LIGHTNING, DROUGHT, LANDSLIDE, HURRICANE, CYCLONE, TYPHOON, TORNADO, NATURAL DISASTER, ACT OF GOD OR THE PUBLIC ENEMY, EPIDEMIC, FAMINE OR PLAGUE, ACTION OF A COURT OR PUBLIC AUTHORITY, CHANGE IN LAW, EXPLOSION, WAR, TERRORISM, ARMED CONFLICT, LABOR STRIKE, LOCKOUT, BOYCOTT OR SIMILAR EVENT BEYOND OUR REASONABLE CONTROL, WHETHER FORESEEN OR UNFORESEEN (EACH A "FORCE MAJEURE EVENT").</p>
						<br>
						<label class="control-form-label text-blue pt-0"><b>7. REFUND REQUEST</b>
						</label>
						<hr class="hr">
						<p class="text-justify uppercase mt-20px">REFUND REQUEST CAN BE SEND AT <a href="cdn-cgi/l/email-protection.html" class="__cf_email__" data-cfemail="86e5e9e8f2e7e5f2a8f6e7e8efe8e2c6e1ebe7efeaa8e5e9eba8">[email&#160;protected]</a> REFUND REQUEST CAN BE COMPLETE WITHIN 5 DAYS OF ONLINE APPLICATION MADE WITH VALID REASON. STANDARD DEDUCTION SHOULD BE APPLIED IF YOU DONT HAVE VALID REASON.</p>
						<br>
						<label class="control-form-label text-blue pt-0"><b>8. CLARIFICATION ABOUT APPLICATION</b>
						</label>
						<hr class="hr">
						<p class="text-justify uppercase mt-20px">IF YOU HAVE ANY QUERY ABOUT APPLICATION PROCESS, YOU CAN WRITE US MAIL <a href="cdn-cgi/l/email-protection.html" class="__cf_email__" data-cfemail="a2c1cdccd6c3c1d68cd2c3cccbccc6e2c5cfc3cbce8cc1cdcf8c">[email&#160;protected]</a> IN CASE WE NEED ANY ADDITIONAL CLARIFICATION ABOUT YOUR PAN APPLICATION, OUR TEAM WILL REACH YOU BY EMAIL OR CALL.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
@include('include.footer')