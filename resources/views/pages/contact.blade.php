@include('include.head')

<body>
	<div class="header" id="">
		<@include('include.header')
	</div>
	<div id="main-content">
		<div id="order-form-thank-you" class="container-fluid p-20px mt-20px">
			<div class="container">
				<div class="col-md-12 text-center text-blue bg-white p-40px box-shadow">
					<div class="col-md-12 text-center">
						<label class="control-form-label text-orange fw-700 fs-24 pt-0">Contact US</label>
						<hr>
						<br>
						<p>Email ID: <b><a class="text-orange td-none" href="">connect.panseva@gmail.com</span></a></b>					
						</p>
						<p>Mo. Number: <b><a class="text-orange td-none" href="">+91 7435 803 802</span></a></b>					
						</p>
						<p>We are complying with the government regulations due to Covid-19. Our support teams are overwhelmed due to the high volumes of Application our executive contact you within 48Hour </p>
						<p class="text-orange">Support hours: Mon-Fri 10AM-6PM</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include('include.footer')