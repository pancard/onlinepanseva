@include('include.head')

<body>
	<div class="header" id="">
		@include('include.header')
	</div>
	<div id="main-content">
		<div id="order-form-thank-you" class="container-fluid p-20px mt-20px">
			<div class="container">
				<div class="col-md-12 text-center text-blue bg-white p-40px box-shadow">
					<div class="col-md-12 text-center">
						<label class="control-form-label text-orange fw-700 fs-24 pt-0">ABOUT US</label>
						<hr>
						<br>
						<p class="uppercase">https://onlinepanseva.in/ a technology based global platform is founded by group of professionals holding expertise in taxation, laws and related consultation on Government Documents. We have introduced https://onlinepanseva.in/ with the aim of serving our clients with best and reliable online assistance and advisory to help Entrepreneurs in setting up and running their business.</p>
						<p class="uppercase">Our goal is to apply strategic thinking, dedicated support, complete efforts and practical approach in order to provide quick, hassle free and automated professional services to our clients.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include('include.footer')
