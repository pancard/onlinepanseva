@include('include.head')

<body onload="">
	<div class="header" id="header">
		@include('include.header')
	</div>
	<div id="main-content">
		<div id="order-form-thank-you" class="container-fluid p-20px mt-20px">
			<div class="container">
				<div class="col-md-12 text-center text-blue bg-white p-40px box-shadow">
					<div class="col-md-12 text-center">
						<label class="control-form-label text-orange fw-700 fs-24 pt-0">PRIVACY POLICY</label>
						<hr>
						<br>
						<p class="text-justify uppercase">This Privacy Policy governs the manner in which https://onlinepanseva.in/ collects, uses, maintains and discloses information collected from users (each, a "User") of the https://onlinepanseva.in/ website ("Site"). This privacy policy applies to the Site and all the information provided on site.</p>
						<br>
						<label class="control-form-label text-blue pt-0"><b>1. PERSONAL IDENTIFICATION INFORMATION</b>
						</label>
						<hr class="hr">
						<p class="text-justify uppercase mt-20px">We may collect personal identification information from Users in a variety of ways, including, but not limited to, when Users visit our site, register on the site, place an application, and in connection with other activities, services, features or resources we make available on our Site. Users may be asked for, as appropriate, name, email address, mailing address, phone number. Users may, however, visit our Site anonymously. We will collect personal identification information from Users only if they voluntarily submit such information to us. Users can always refuse to supply personally identification information, except that it may prevent them from engaging in certain Site related activities.</p>
						<br>
						<label class="control-form-label text-blue pt-0"><b>2. NON-PERSONAL IDENTIFICATION INFORMATION</b>
						</label>
						<hr class="hr">
						<p class="text-justify uppercase mt-20px">When you visit our website, we may collect some basic Non-Personally Identifiable Information about you. We collect non-personally identifiable information about you in a number of ways, including tracking your activities through Your IP address, computer settings, or most-recently visited URL.</p>
						<p class="text-justify uppercase mt-20px">Non-Personally Identifiable Information is collected in order to provide you with satisfactory service and to improve our website. We may use the information to detect problems with our server and to administer our website. In addition, Non-Personally Identifiable Information is compiled by us and analysed on an aggregate basis.</p>
						<p class="text-justify uppercase mt-20px">By using this Website, You agree to the publication of your IP address on this Website.</p>
						<br>
						<label class="control-form-label text-blue pt-0"><b>3. WEB BROWSER COOKIES</b>
						</label>
						<hr class="hr">
						<p class="text-justify uppercase mt-20px">Our Site may use "cookies" to enhance User experience. User's web browser places cookies on their hard drive for record-keeping purposes and sometimes to track information about them. User may choose to set their web browser to refuse cookies, or to alert you when cookies are being sent. If they do so, note that some parts of the Site may not function properly.</p>
						<br>
						<label class="control-form-label text-blue pt-0"><b>4. HOW WE PROTECT YOUR INFORMATION</b>
						</label>
						<hr class="hr">
						<p class="text-justify uppercase mt-20px">We adopt appropriate data collection, storage and processing practices and security measures to protect against unauthorized access, alteration, disclosure or destruction of your personal information, transaction information and data stored on our Site.</p>
						<p class="text-justify uppercase">Sensitive and private data exchange between the Site and its Users happens over a SSL secured communication channel and is encrypted and protected with digital signatures. Our Site is also in compliance with PCI vulnerability standards in order to create as secure of an environment as possible for Users.</p>
						<br>
						<label class="control-form-label text-blue pt-0"><b>5. COMPLIANCE WITH CHILDREN'S ONLINE PRIVACY PROTECTION ACT</b>
						</label>
						<hr class="hr">
						<p class="text-justify uppercase mt-20px">Protecting the privacy of the very young is especially important. For that reason, we never collect or maintain information at our Site from those we actually know are under 13, and no part of our website is structured to attract anyone under 13.</p>
						<br>
						<label class="control-form-label text-blue pt-0"><b>6. CHANGES TO THIS PRIVACY POLICY</b>
						</label>
						<hr class="hr">
						<p class="text-justify uppercase mt-20px">https://onlinepanseva.in/ have the discretion to update this privacy policy at any time. We encourage Users to frequently check this page for any changes to stay informed about how we are helping to protect the personal information we collect. You acknowledge and agree that it is your responsibility to review this privacy policy periodically and become aware of modifications.</p>
						<br>
						<label class="control-form-label text-blue pt-0"><b>7. COMMUNICATION POLICY</b>
						</label>
						<hr class="hr">
						<p class="text-justify uppercase mt-20px">https://onlinepanseva.in/ may send alerts to the mobile phone number provided by You, while registering with the https://onlinepanseva.in/ Platform for the service or on any updated mobile number subsequently provided by You on the https://onlinepanseva.in/ Platform, or via e-mail. The alerts will be received in case of SMS, only if the mobile phone is in ‘On’ mode to receive the SMS, in case of e-mail, only if the e-mail servers and e-mail ids are functional. If the mobile phone is in ‘Off’ mode or if the e-mail servers or ids are not functional, then You may not get the alert at all or get delayed messages.</p>
						<p class="text-justify uppercase mt-20px">https://onlinepanseva.in/ will make best efforts to provide alerts via SMS/e-mail and it shall be deemed that You shall have received the information sent from https://onlinepanseva.in/ as an alert on the mobile phone number or e-mail id provided during the course of, or in relation to, using the https://onlinepanseva.in/ Platform or availing any https://onlinepanseva.in/ services. https://onlinepanseva.in/ shall not be under any obligation to confirm the authenticity of the person(s) receiving the alert. You cannot hold https://onlinepanseva.in/ liable for non-availability of the SMS/email alert service in any manner whatsoever.</p>
						<p class="text-justify uppercase mt-20px">The SMS/e-mail alert service provided by https://onlinepanseva.in/ is an additional facility provided for Your convenience and that it may be susceptible to error, omission and/or inaccuracy. In the event that You observe any error in the information provided in the alert, https://onlinepanseva.in/ shall be immediately informed about the same by You and https://onlinepanseva.in/ will make best possible efforts to rectify the error as early as possible. You shall not hold https://onlinepanseva.in/ liable for any loss, damages, claim, expense including legal cost that may be incurred/suffered by You on account of the SMS/e-mail alert facility.</p>
						<p class="text-justify uppercase mt-20px">The clarity, readability, accuracy and promptness of providing the SMS/e-mail alert service depend on many factors including the infrastructure and connectivity of the service provider. https://onlinepanseva.in/ shall not be responsible for any non-delivery, delayed delivery or distortion of the alert in any way whatsoever.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
@include('include.footer')