@include('include.head')

<body>
	<div class="header" id="header">
		@include('include.header')
	</div>
	<div id="main-content">
		<div id="order-form-thank-you" class="container-fluid p-20px mt-20px">
			<div class="container">
				<div class="col-md-12 text-center text-blue bg-white p-40px box-shadow">
					<div class="col-md-12 text-center">
						<label class="control-form-label text-orange fw-700 fs-24 pt-0">DISCLAIMER POLICY</label>
						<hr>
						<br>
						<p class="text-justify">PLEASE READ THIS DOCUMENT CAREFULLY BEFORE ACCESSING OR USING THE SITE. BY ACCESSING OR USING THE SITE, YOU AGREE TO BE BOUND BY THE TERMS AND CONDITIONS SET FORTH BELOW. IF YOU DO NOT WISH TO BE BOUND BY THESE TERMS AND CONDITIONS, YOU MAY NOT ACCESS OR USE THE SITE. https://onlinepanseva.in/ MAY MODIFY THIS AGREEMENT AT ANY TIME, AND SUCH MODIFICATIONS SHALL BE EFFECTIVE IMMEDIATELY UPON POSTING OF THE MODIFIED AGREEMENT ON THE SITE. YOU AGREE TO REVIEW THE AGREEMENT PERIODICALLY TO BE AWARE OF SUCH MODIFICATIONS AND YOUR CONTINUED ACCESS OR USE OF THE SITE SHALL BE DEEMED YOUR CONCLUSIVE ACCEPTANCE OF THE MODIFIED AGREEMENT.</p>
						<br>
						<label class="control-form-label text-blue pt-0"><b>1. USE OF THE SITE</b>
						</label>
						<hr class="hr">
						<p class="text-justify mt-20px">YOU UNDERSTAND THAT, EXCEPT FOR INFORMATION, PRODUCTS OR SERVICES CLEARLY IDENTIFIED AS BEING SUPPLIED BY https://onlinepanseva.in/, WE DOES NOT OPERATE, CONTROL OR ENDORSE ANY INFORMATION, PRODUCTS OR SERVICES ON THE INTERNET IN ANY WAY.</p>
						<p class="text-justify">YOU ALSO UNDERSTAND THAT https://onlinepanseva.in/ CANNOT AND DOES NOT GUARANTEE OR WARRANT THAT FILES AVAILABLE FOR DOWNLOADING THROUGH THE SITE WILL BE FREE OF INFECTION OR VIRUSES, WORMS, TROJAN HORSES OR OTHER CODE THAT MANIFEST CONTAMINATING OR DESTRUCTIVE PROPERTIES. YOU ARE RESPONSIBLE FOR IMPLEMENTING SUFFICIENT PROCEDURES AND CHECKPOINTS TO SATISFY YOUR PARTICULAR REQUIREMENTS FOR ACCURACY OF DATA INPUT AND OUTPUT, AND FOR MAINTAINING A MEANS EXTERNAL TO THE SITE FOR THE RECONSTRUCTION OF ANY LOST DATA.</p>
						<p class="text-justify">YOU ASSUME TOTAL RESPONSIBILITY AND RISK FOR YOUR USE OF THE SITE AND THE INTERNET. https://onlinepanseva.in/ PROVIDES THE SITE AND RELATED INFORMATION "AS IS" AND DOES NOT MAKE ANY EXPRESS OR IMPLIED WARRANTIES, REPRESENTATIONS OR ENDORSEMENTS WHATSOEVER (INCLUDING WITHOUT LIMITATION WARRANTIES OF TITLE OR NONINFRINGEMENT, OR THE IMPLIED WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE) WITH REGARD TO THE SERVICE, ANY MERCHANDISE INFORMATION OR SERVICE PROVIDED THROUGH THE SERVICE OR ON THE INTERNET GENERALLY, AND https://onlinepanseva.in/ SHALL NOT BE LIABLE FOR ANY COST OR DAMAGE ARISING EITHER DIRECTLY OR INDIRECTLY FROM ANY SUCH TRANSACTION. IT IS SOLELY YOUR RESPONSIBILITY TO EVALUATE THE ACCURACY, COMPLETENESS AND USEFULNESS OF ALL OPINIONS, ADVICE, SERVICES, MERCHANDISE AND OTHER INFORMATION PROVIDED THROUGH THE SERVICE OR ON THE INTERNET GENERALLY. https://onlinepanseva.in/ DOES NOT WARRANT THAT THE SERVICE WILL BE UNINTERRUPTED OR ERROR-FREE OR THAT DEFECTS IN THE SERVICE WILL BE CORRECTED. YOU UNDERSTAND FURTHER THAT THE PURE NATURE OF THE INTERNET CONTAINS UNEDITED MATERIALS SOME OF WHICH ARE SEXUALLY EXPLICIT OR MAY BE OFFENSIVE TO YOU. YOUR ACCESS TO SUCH MATERIALS IS AT YOUR RISK. https://onlinepanseva.in/ HAS NO CONTROL OVER AND ACCEPTS NO RESPONSIBILITY WHATSOEVER FOR SUCH MATERIALS.</p>
						<br>
						<label class="control-form-label text-blue pt-0"><b>2. LIMITATION OF LIABILITY</b>
						</label>
						<hr class="hr">
						<p class="text-justify mt-20px">IN NO EVENT WILL https://onlinepanseva.in/ BE LIABLE FOR (I) ANY INCIDENTAL, CONSEQUENTIAL, OR INDIRECT DAMAGES (INCLUDING, BUT NOT LIMITED TO, DAMAGES FOR LOSS OF PROFITS, BUSINESS INTERRUPTION, LOSS OF PROGRAMS OR INFORMATION, AND THE LIKE) ARISING OUT OF THE USE OF OR INABILITY TO USE THE SERVICE, OR ANY INFORMATION, OR TRANSACTIONS PROVIDED ON THE SERVICE, OR DOWNLOADED FROM THE SERVICE, OR ANY DELAY OF SUCH INFORMATION OR SERVICE. EVEN IF https://onlinepanseva.in/ OR ITS AUTHORIZED REPRESENTATIVES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES, OR (II) ANY CLAIM ATTRIBUTABLE TO ERRORS, OMISSIONS, OR OTHER INACCURACIES IN THE SERVICE AND/OR MATERIALS OR INFORMATION DOWNLOADED THROUGH THE SERVICE. BECAUSE SOME STATES DO NOT ALLOW THE EXCLUSION OR LIMITATION OF LIABILITY FOR CONSEQUENTIAL OR INCIDENTAL DAMAGES, THE ABOVE LIMITATION MAY NOT APPLY TO YOU. IN SUCH STATES, https://onlinepanseva.in/ LIABILITY IS LIMITED TO THE GREATEST EXTENT PERMITTED BY LAW.</p>
						<br>
						<label class="control-form-label text-blue pt-0"><b>3. INDEMNIFICATION</b>
						</label>
						<hr class="hr">
						<p class="text-justify mt-20px">YOU AGREE TO INDEMNIFY, DEFEND AND HOLD HARMLESS https://onlinepanseva.in/, ITS OFFICERS, EMPLOYEES AND ANY THIRD PARTY INFORMATION PROVIDERS TO THE SERVICE FROM AND AGAINST ALL LOSSES, EXPENSES, DAMAGES AND COSTS, INCLUDING REASONABLE ATTORNEYS' FEES, RESULTING FROM ANY VIOLATION OF THIS AGREEMENT (INCLUDING NEGLIGENT OR WRONGFUL CONDUCT) BY YOU OR ANY OTHER PERSON ACCESSING THE SERVICE.</p>
						<br>
						<label class="control-form-label text-blue pt-0"><b>4. MISCELLANEOUS</b>
						</label>
						<hr class="hr">
						<p class="text-justify mt-20px">THIS AGREEMENT SHALL ALL BE GOVERNED AND CONSTRUED IN ACCORDANCE WITH THE LAWS OF INDIA APPLICABLE TO AGREEMENTS MADE AND TO BE PERFORMED IN INDIA. YOU AGREE THAT ANY LEGAL ACTION OR PROCEEDING BETWEEN https://onlinepanseva.in/ AND YOU FOR ANY PURPOSE CONCERNING THIS AGREEMENT OR THE PARTIES' OBLIGATIONS HEREUNDER SHALL BE BROUGHT EXCLUSIVELY IN A FEDERAL OR STATE COURT OF COMPETENT JURISDICTION SITTING IN INDIA. ANY CAUSE OF ACTION OR CLAIM YOU MAY HAVE WITH RESPECT TO THE SERVICE MUST BE COMMENCED WITHIN ONE (1) YEAR AFTER THE CLAIM OR CAUSE OF ACTION ARISES OR SUCH CLAIM OR CAUSE OF ACTION IS BARRED.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
@include('include.footer')