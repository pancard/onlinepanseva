@include('include.head')

<body>
    <div class="header" id="">
        @include('include.header')
        </div>
        <div id="main-content">
            <div id="order-form-thank-you" class="container-fluid p-20px mt-20px">
                <div class="container-fluid">
                            <label class="control-form-label text-orange fw-700 fs-24 pt-0">DOCUMENTS REQUIRED - INDIA</label>
                            <hr>
                            <br>
                            <div class="container">
                                <ul id="tabs" class="nav nav-tabs" role="tablist">
                                    <li class="nav-item">
                                        <a id="tab-A" href="#pane-A" class="nav-link active" data-toggle="tab" role="tab">Proof of Identity</a>
                                    </li>
                                
                                </ul>
                                <div id="content" class="tab-content" role="tablist">
                                        <div class="collapse show" role="tabpanel" aria-labelledby="heading-A">
                                            <div class="card-body text-justify">
                                                <p>You Should Have Any One Of The Following Documents As A Proof Of Identity For Pan Application :</p>
                                                <ul>
                                                    <li>AADHAAR Card issued by UIDAI (In Copy)</li>
                                                    <li>Arms License (In Copy)</li>
                                                    <li>Bank certificate in original on letter head of the branch (along with name & stamp of issuing officer) containing duly attested photograph and bank A/c no of the Applicant. <a class="text-orange td-none" href="assets/downloads/Prescribed_format_for_Bank_Certificate.pdf" download="Prescribed Format For Bank Certificate.pdf"><b>Click here to Download the Format</b></a>
                                                    </li>
                                                    <li>Central Govt Health Scheme Card or Ex-serviceman Contributory Health Scheme photo card (In Copy)</li>
                                                    <li>Certificate of identity in original signed by MP or MLA or Municipal Councilor (Annexure-A) . <a class="text-orange td-none" href="assets/downloads/Prescribed_format_for_Verification_Certificate.pdf" download="Prescribed Format For Verification Certificate.pdf"><b>Click here to Download the Format</b></a>
                                                    </li>
                                                    <li>Certificate of identity in original signed from Gazetted Officer . <a class="text-orange td-none" href="assets/downloads/Prescribed_format_for_Verification_Certificate.pdf" download="Prescribed Format For Verification Certificate.pdf"><b>Click here to Download the Format</b></a>
                                                    </li>
                                                    <li>Driving License (In Copy)</li>
                                                    <li>Passport (In Copy)</li>
                                                    <li>Pensioner Card having photograph of Applicant (In Copy)</li>
                                                    <li>Photo ID card issued by Central Govt or State Govt or a Public Sector Undertaking (In Copy)</li>
                                                    <li>Ration Card having photograph of the applicant. (In Copy)</li>
                                                    <li>Voters ID card (In Copy)</li>
                                                </ul>
                                                
                                            </div>
                                            <h3>gsdg</h3>
                                            <div class="card-body text-justify">
                                                <p>You Should Have Any One Of The Following Documents As A Proof Of Address For Pan Application :</p>
                                                <ul>
                                                    <li>AADHAAR Card issued by UIDAI (In Copy)</li>
                                                    <li>Allotment letter of accomodation issued by Central Govt or State Govt not more than 3 Years old (In Copy)</li>
                                                    <li>Bank Account Statement not more than 3 months old (In Copy)</li>
                                                    <li>Certificate of address in original signed by MP or MLA or Municipal Councilor (Annexure-A). <a class="text-orange td-none" href="assets/downloads/Prescribed_format_for_Verification_Certificate.pdf" download="Prescribed Format For Verification Certificate.pdf"><b>Click here to Download the Format</b></a>
                                                    </li>
                                                    </li>
                                                    <li>Certificate of address in original signed from Gazetted Officer. <a class="text-orange td-none" href="assets/downloads/Prescribed_format_for_Verification_Certificate.pdf" download="Prescribed Format For Verification Certificate.pdf"><b>Click here to Download the Format</b></a>
                                                    </li>
                                                    </li>
                                                    <li>Consumer Gas connection card or book or piped gas bill not more than 3 months old (In Copy)</li>
                                                    <li>Copy Electricity bill not more than 3 months old.</li>
                                                    <li>Copy Land Line telephone Bill or Broad band connection bill not more than 3 months old.</li>
                                                    <li>Credit card statement not more than 3 months old (In Copy)</li>
                                                    <li>Depository account statement not more than 3 months old (In Copy)</li>
                                                    <li>Domicile certificate issued by Government (In Copy)</li>
                                                    <li>Driving License (In Copy)</li>
                                                    <li>Employer certificate in ORIGINAL . <a class="text-orange td-none" href="assets/downloads/Prescribed_format_for_Employer_Certificate.pdf" download="Prescribed Format For Employer Certificate.pdf"><b>Click here to Download the Format</b></a>
                                                    </li>
                                                    </li>
                                                    <li>Latest Property tax assessment order (In Copy)</li>
                                                    <li>Passport (In Copy)</li>
                                                    <li>Passport of the Spouse (In Copy)</li>
                                                    <li>Post office pass book having address of applicant (In Copy)</li>
                                                    <li>Property registration document (In Copy)</li>
                                                    <li>Voters ID card (In Copy)</li>
                                                    <li>Water Bill not more than 3 months old (In Copy)</li>
                                                </ul>
                                                <p><b>Note:</b>
                                                </p>
                                                <p>Proofs having initials in Name and not having applicant's Complete Name are not acceptable except Voter ID Card. Driving license is acceptable only when short form is there in Middle Name.</p>
                                                <p>Representative Assessee (RA) of Minor is mandatory and should be Father/Mother only. RA is required to sign on all places on the form wherever applicable for Minors and Others filing the RA.</p>
                                                <p>Applicants who want the Delivery of PAN Card at Office Address, Should send Employee Certificate in Original as per the format attached above.</p>
                                            </div>
                                        </div>
                                    </div>
                                   
                                        <div class="collapse" role="tabpanel" aria-labelledby="heading-B">
                                            <div class="card-body text-justify">
                                                <p>You Should Have Any One Of The Following Documents As A Proof Of Address For Pan Application :</p>
                                                <ul>
                                                    <li>AADHAAR Card issued by UIDAI (In Copy)</li>
                                                    <li>Allotment letter of accomodation issued by Central Govt or State Govt not more than 3 Years old (In Copy)</li>
                                                    <li>Bank Account Statement not more than 3 months old (In Copy)</li>
                                                    <li>Certificate of address in original signed by MP or MLA or Municipal Councilor (Annexure-A). <a class="text-orange td-none" href="assets/downloads/Prescribed_format_for_Verification_Certificate.pdf" download="Prescribed Format For Verification Certificate.pdf"><b>Click here to Download the Format</b></a>
                                                    </li>
                                                    </li>
                                                    <li>Certificate of address in original signed from Gazetted Officer. <a class="text-orange td-none" href="assets/downloads/Prescribed_format_for_Verification_Certificate.pdf" download="Prescribed Format For Verification Certificate.pdf"><b>Click here to Download the Format</b></a>
                                                    </li>
                                                    </li>
                                                    <li>Consumer Gas connection card or book or piped gas bill not more than 3 months old (In Copy)</li>
                                                    <li>Copy Electricity bill not more than 3 months old.</li>
                                                    <li>Copy Land Line telephone Bill or Broad band connection bill not more than 3 months old.</li>
                                                    <li>Credit card statement not more than 3 months old (In Copy)</li>
                                                    <li>Depository account statement not more than 3 months old (In Copy)</li>
                                                    <li>Domicile certificate issued by Government (In Copy)</li>
                                                    <li>Driving License (In Copy)</li>
                                                    <li>Employer certificate in ORIGINAL . <a class="text-orange td-none" href="assets/downloads/Prescribed_format_for_Employer_Certificate.pdf" download="Prescribed Format For Employer Certificate.pdf"><b>Click here to Download the Format</b></a>
                                                    </li>
                                                    </li>
                                                    <li>Latest Property tax assessment order (In Copy)</li>
                                                    <li>Passport (In Copy)</li>
                                                    <li>Passport of the Spouse (In Copy)</li>
                                                    <li>Post office pass book having address of applicant (In Copy)</li>
                                                    <li>Property registration document (In Copy)</li>
                                                    <li>Voters ID card (In Copy)</li>
                                                    <li>Water Bill not more than 3 months old (In Copy)</li>
                                                </ul>
                                                <p><b>Note:</b>
                                                </p>
                                                <p>Proofs having initials in Name and not having applicant's Complete Name are not acceptable except Voter ID Card. Driving license is acceptable only when short form is there in Middle Name.</p>
                                                <p>Representative Assessee (RA) of Minor is mandatory and should be Father/Mother only. RA is required to sign on all places on the form wherever applicable for Minors and Others filing the RA.</p>
                                                <p>Applicants who want the Delivery of PAN Card at Office Address, Should send Employee Certificate in Original as per the format attached above.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="pane-d" class="card tab-pane fade" role="tabpanel" aria-labelledby="tab-d">
                                        <div class="card-header" role="tab" id="heading-d">
                                            <h5 class="mb-0">
<a class="collapsed" data-toggle="collapse" href="#collapse-d" data-parent="#content" aria-expanded="false" aria-controls="collapse-d">
Collapsible Group Item C
</a>
</h5>
                                        </div>
                                        <div id="collapse-C" class="collapse" role="tabpanel" aria-labelledby="heading-C">
                                            <div class="card-body text-justify">
                                                <p>You Should Have The Following Documents As A Proof Of Huf For Pan Application :</p>
                                                <ul>
                                                    <li>Hindu Undivided Family (HUF) Notarized Affidavit on stamp paper of Rs. 20 Click here to Download the Format (MANDATORY)</li>
                                                    <li>Along with HUF Affidavit - One ID, Address & DOB Proof of Karta is Required.</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="pane-C" class="card tab-pane fade" role="tabpanel" aria-labelledby="tab-C">
                                        <div class="card-header" role="tab" id="heading-C">
                                        </div>
                                        <div class="collapse" role="tabpanel" aria-labelledby="heading-C">
                                            <div class="card-body text-justify">
                                                <p>You Should Have Any One Of The Following Documents As A Proof Of Dob For Pan Application :</p>
                                                <ul>
                                                    <li>AADHAAR Card issued by UIDAI (In Copy)</li>
                                                    <li>Affidavit sworn before Magistrate stating Date of Birth (In Copy)</li>
                                                    <li>Birth Certificate issued by Municipal Authority or any office authorized to issue Birth & Death Certificate by Registrar of Birth and Deaths or the Indian Consulate (In Copy)</li>
                                                    <li>Central Govt Health Scheme Card or Ex serviceman Contributory Health Scheme photo card (In Copy)</li>
                                                    <li>Domicile certificate issued by Government (In Copy)</li>
                                                    <li>Driving License (In Copy)</li>
                                                    <li>Marriage Certificate issued by Registrar of Marriages (In Copy)</li>
                                                    <li>Matriculation certificate or mark sheet of recognised board (In Copy)</li>
                                                    <li>Passport (In Copy)</li>
                                                    <li>Pension payment order (In Copy)</li>
                                                    <li>Photo ID card issued by Central Govt or State Govt or a Public Sector undertaking or State Public sector undertaking (In Copy)</li>
                                                    <li>Voters ID card (In Copy)</li>
                                                </ul>
                                                <p><b>Note:</b>
                                                </p>
                                                <p>Proofs having initials in Name and not having applicant's Complete Name are not acceptable except Voter ID Card. Driving license is acceptable only when short form is there in Middle Name.</p>
                                                <p>Representative Assessee (RA) of Minor is mandatory and should be Father/Mother only. RA is required to sign on all places on the form wherever applicable for Minors and Others filing the RA.</p>
                                                <p>DOB Proof should have complete DOB present i.e. Day, Month and Year should be present on DOB Proof provided.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
@include('include.footer')