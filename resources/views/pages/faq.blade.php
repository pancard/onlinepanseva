@include('include.head')

<body>
	<div class="header" id="">
		@include('include.header')
	</div>
	<div id="main-content">
		<div id="order-form-thank-you" class="container-fluid p-20px mt-20px">
			<div class="container-fluid">
				<div class="col-md-12 text-blue bg-white p-40px box-shadow">
					<div class="col-md-12">
						<div class="col-md-12 text-center">
							<label class="control-form-label text-orange fw-700 fs-24 pt-0">Frequently Asked Question</label>
						</div>
						<hr>
						<div id="accordion" class="row mt-20px">
							<div class="col-md-6">
								<div class="card mt-10px">
									<a class="card-link" data-toggle="collapse" href="#msmeOne">
										<div class="card-header text-blue fw-700">What is a PAN card?</div>
									</a>
									<div id="msmeOne" class="collapse" data-parent="#accordion">
										<div class="card-body">
											<p>PAN is a short form of Permanent Account Number. Permanent Account Number (PAN) is a ten-digit alphanumeric number, issued by the Income Tax Department, to any “person” who applies for it. PAN acts as an identifier for the “person” with the tax department.</p>
											<p>A typical PAN is BPQPN8657W.</p>
											<p>First three characters i.e. “BPQ” in the above PAN are alphabetic series running from AAA to ZZZ. Fifth character i.e. “N” in the above PAN represents first character of the PAN holder’s last name/surname. Last character i.e. “W” in the above PAN is an alphabetic check digit.</p>
										</div>
									</div>
								</div>
								<div class="card mt-10px">
									<a class="card-link" data-toggle="collapse" href="#msmeThree">
										<div class="card-header text-blue fw-700">What is the Validity period of PAN?</div>
									</a>
									<div id="msmeThree" class="collapse" data-parent="#accordion">
										<div class="card-body">PAN issued by the Income Tax Department will be valid for lifetime.</div>
									</div>
								</div>
								<div class="card mt-10px">
									<a class="card-link" data-toggle="collapse" href="#msmeFive">
										<div class="card-header text-blue fw-700">Can a person obtain more than one PAN?</div>
									</a>
									<div id="msmeFive" class="collapse" data-parent="#accordion">
										<div class="card-body">Obtaining or possessing more than one PAN is against the law. If any person obtained more than one Pan, he has to make application for cancellation of PAN.</div>
									</div>
								</div>
								<div class="card mt-10px">
									<a class="card-link" data-toggle="collapse" href="#msmeSeven">
										<div class="card-header text-blue fw-700">How does Income Tax Department ensure that PAN is quoted on transactions mentioned above?</div>
									</a>
									<div id="msmeSeven" class="collapse" data-parent="#accordion">
										<div class="card-body">It is statutory responsibility of a person receiving document relating to economic or financial transactions notified by the CBDT to ensure that PAN has been duly quoted in the document.</div>
									</div>
								</div>
								<div class="card mt-10px">
									<a class="card-link" data-toggle="collapse" href="#msmeNine">
										<div class="card-header text-blue fw-700">What is the procedure for applicants who cannot sign?</div>
									</a>
									<div id="msmeNine" class="collapse" data-parent="#accordion">
										<div class="card-body">In the interest of environment no physical copy of MSME Certificate will be issued. Government believes in paperless work</div>
									</div>
								</div>
								<div class="card mt-10px">
									<a class="card-link" data-toggle="collapse" href="#msme11">
										<div class="card-header text-blue fw-700">Which documents will serve as Proof of Address for individual Indian citizens residing in India?</div>
									</a>
									<div id="msme11" class="collapse" data-parent="#accordion">
										<div class="card-body">
											<p>Individual Indian citizens residing in India can provide photocopy of any one of the following as Proof of Address:</p>
											<ul>
												<li>Voter Identity Card</li>
												<li>Valid Driving License</li>
												<li>Aadhaar card issued by the Unique Identification Authority of India</li>
												<li>Valid Passport</li>
												<li>3 Months Bank Account Statement/Passbook</li>
												<li>3 Months Credit Card Statement</li>
												<li>Ration Card</li>
												<li>3 Months Telephone Bill</li>
												<li>Valid Employer Certificate</li>
												<li>3 Months Electricity Bill</li>
												<li>Valid Depository Account Statement</li>
												<li>Property Tax Assessment Order</li>
												<li>Certificate of Address signed by a Gazetted Officer or Member of Parliament or Member of Legislative Assembly or Municipal Councillor.</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="card mt-10px">
									<a class="card-link" data-toggle="collapse" href="#msmeTwo">
										<div class="card-header text-blue fw-700">Is Aadhaar number is mandatory for making PAN Application?</div>
									</a>
									<div id="msmeTwo" class="collapse" data-parent="#accordion">
										<div class="card-body">
											<p>Yes, it is mandatory to mention Aadhaar Number in Application form, if the applicant is individual and Indian.</p>
										</div>
									</div>
								</div>
								<div class="card mt-10px">
									<a class="card-link" data-toggle="collapse" href="#msmeFour">
										<div class="card-header text-blue fw-700">Is there any TATKAL facility for allotment of PAN?</div>
									</a>
									<div id="msmeFour" class="collapse" data-parent="#accordion">
										<div class="card-body">
											<p>No such facility is available.</p>
										</div>
									</div>
								</div>
								<div class="card mt-10px">
									<a class="card-link" data-toggle="collapse" href="#msmeSix">
										<div class="card-header text-blue fw-700">Why is it necessary to have pan?</div>
									</a>
									<div id="msmeSix" class="collapse" data-parent="#accordion">
										<div class="card-body">
											<p>It is mandatory to quote PAN on return of income, all correspondence with any income tax authority. Since 1 January 2005 it has become mandatory to quote PAN on challans for any payments due to Income Tax Department.</p>
										</div>
									</div>
								</div>
								<div class="card mt-10px">
									<a class="card-link" data-toggle="collapse" href="#msmeEight">
										<div class="card-header text-blue fw-700">Can an application for PAN be made through Internet?</div>
									</a>
									<div id="msmeEight" class="collapse" data-parent="#accordion">
										<div class="card-body">
											<p>Yes, application for fresh allotment of PAN can be made through Internet. Further, requests for changes or correction in PAN data or request for reprint of PAN card (for an existing PAN) may also be made through Internet.</p>
										</div>
									</div>
								</div>
								<div class="card mt-10px">
									<a class="card-link" data-toggle="collapse" href="#msme10">
										<div class="card-header text-blue fw-700">Which documents will serve as Proof of Identity for individual Indian citizens residing in India?</div>
									</a>
									<div id="msme10" class="collapse" data-parent="#accordion">
										<div class="card-body">
											<p>Individual Indian citizens residing in India can provide photocopy of any one of the following as Proof of Identity.</p>
											<ul>
												<li>Voter Identity Card</li>
												<li>Driving License</li>
												<li>Passport</li>
												<li>Aadhaar card issued by the Unique Identification Authority of India</li>
												<li>Arm’s License</li>
												<li>Photo identity card issued by the Central Government or a State Government or a Public Sector Undertaking</li>
												<li>Pensioner Card having photograph of the applicant</li>
												<li>Central Government Health Scheme Card or Ex-servicemen Contributory Heath Scheme photo card</li>
												<li>Ration Card having photograph of applicant</li>
											</ul>
										</div>
									</div>
								</div>
								<div class="card mt-10px">
									<a class="card-link" data-toggle="collapse" href="#msmeTwelve">
										<div class="card-header text-blue fw-700">I have been allotted more than one PAN card, what is the procedure for cancellation of other PAN card(s)?</div>
									</a>
									<div id="msmeTwelve" class="collapse" data-parent="#accordion">
										<div class="card-body">
											<p>You have to mention the PAN card number which you are using currently on top of the application form. All other PAN card/s inadvertently allotted to you should be mentioned in the online form after clicking yes on the question - Are you having more than 1 PAN card - inadvertently allotted PAN card(s) should be sent for cancellation along with the form.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@include('include.footer')