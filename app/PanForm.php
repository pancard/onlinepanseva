<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PanForm extends Model
{
    public $timestamps = false;
    protected $table = 'tbl_pandata'; 
    
    protected $fillable = [

        'pan_id',
        'applicant_name_title', 
        'applicant_first_name',
        'applicant_middle_name',
        'applicant_last_name',

        'father_first_name', 
        'father_middle_name',
        'father_last_name',

        'mother_first_name',
        'mother_middle_name',
        'mother_last_name',

        'applicant_mobile_no',
        'applicant_email_id',
        'date_of_birth', 
        'applicant_aadhaar_no',
        'name_of_aadhaar',
        'aadhaar_proof',
        'gender',
        'source_of_income',
        'communication_address',

        'house_no',
        'area_street',
        'state_union',
        'city_district',
        'pin_zip',
        'country',

        'office_name',
        'oa_house_no',
        'oa_area_street',
        'oa_state_union',
        'oa_city_district',
        'oa_pin_zip',
        'oa_country',
        'off_addr_proof',
        'repre_assess',

        'id_proof',
        'dob_proof',
        'addr_proof',


    ];
}

