<?php

namespace App\Http\Controllers;

use DB;
use App\PanForm;
use App\Transactions;
use Illuminate\Http\Request;
use App\Http\Controllers\Date;
use Razorpay\Api\Api;



class DataController extends Controller
{

    private $transactions;

    public function __construct(Transactions $transactions)
    {

        $this->transactions = $transactions;

    }

    public function store(Request $request)
    {
        
    	$pan_id = "PAN_" .rand("11111111","99999999");

        //dd($insertdata);
        $insertdata = new PanForm([

        	'pan_id' => $pan_id,
            'applicant_name_title' => $request->get('applicant_name_title'),
            'applicant_first_name' => $request->get('applicant_first_name'),
            'applicant_middle_name' => $request->get('applicant_middle_name'),
            'applicant_last_name' => $request->get('applicant_last_name'),

            'father_first_name' => $request->get('father_first_name'),
            'father_middle_name' => $request->get('father_middle_name'),
            'father_last_name' => $request->get('father_last_name'),

            'mother_first_name' => $request->get('mother_first_name'),
            'mother_middle_name' => $request->get('mother_middle_name'),
            'mother_last_name' => $request->get('mother_last_name'),

            'applicant_mobile_no' => $request->get('applicant_mobile_no'),
            'applicant_email_id' => $request->get('applicant_email_id'),
            'date_of_birth' => $request->get('date_of_birth'),
            'applicant_aadhaar_no' => $request->get('applicant_aadhaar_no'),
            'name_of_aadhaar' => $request->get('name_of_aadhaar'),
            'aadhaar_proof' => $request->get('aadhaar_proof'),
            'gender' => $request->get('gender'),
            'source_of_income' => $request->get('source_of_income'),
            'communication_address' => $request->get('communication_address'),

            'house_no' => $request->get('house_no'),
            'area_street' => $request->get('area_street'),
            'state_union' => $request->get('state_union'),
            'city_district' => $request->get('city_district'),
            'pin_zip' => $request->get('pin_zip'),
            'country' => $request->get('country'),


            'office_name' => $request->get('office_name'),
            'oa_house_no' => $request->get('oa_house_no'),
            'oa_area_street' => $request->get('oa_area_street'),
            'oa_state_union' => $request->get('oa_state_union'),
            'oa_city_district' => $request->get('oa_city_district'),
            'oa_pin_zip' => $request->get('oa_pin_zip'),
            'oa_country' => $request->get('oa_country'),
            'off_addr_proof' => $request->get('off_addr_proof'),
            'repre_assess' => $request->get('repre_assess'),

            'id_proof' => $request->get('id_proof'),
            'dob_proof' => $request->get('dob_proof'),
            'addr_proof' => $request->get('addr_proof'),
            

        ]);
        
        $insertdata->save();

    
        return redirect('/makepayment/'.$pan_id);
    }

    public function makepayment($pan_id)
    {            

        $key_id = "rzp_live_MHIB7VJ4qhRsWM";
        $secret = "fjtFUsmKIajEvkMosuPdDNae";
        $amount  = 107;
        $api = new Api($key_id, $secret);

        $info = DB::table('tbl_pandata')
                    ->where('tbl_pandata.pan_id', $pan_id)   
                    ->first();

        $order = $api->order->create([
                    'receipt' => $pan_id,
                    'amount'  => $amount * 100,
                    'currency' => 'INR',
                    'payment_capture' => 1 
                    ]); 

        $order_id = $order['id'];
        $first_name = $info->applicant_first_name;
        $email = $info->applicant_email_id;
        $mobile_number = $info->applicant_mobile_no;

        $result = $this->transactions->addTransaction($pan_id, $order_id, $first_name, $email, $mobile_number);                         

        return view('pages.makepayment', compact('result', 'info'));  
    }


    public function updateTransaction(Request $request)
    {

        
        $key_id = "rzp_live_MHIB7VJ4qhRsWM";
        $key_secret = "fjtFUsmKIajEvkMosuPdDNae";

        /*$signature = $request->get('razorpay_signature');
        $payment_id = $request->get('razorpay_payment_id');
        $order_id = $request->get('razorpay_order_id');*/
        $payment_id = $request->get('razorpay_payment_id');

        
        $api = new Api($key_id, $key_secret);
        $payment = $api->payment->fetch($payment_id);


        $order_id = $this->transactions->updateTransaction($payment);

        $data = $this->transactions->where('order_id', $order_id)->first();
        
        if(isset($data)){

            return view('pages.success', compact('data'));  

        }else{
            dd('Error 404');
        }
        
        
    }

}

