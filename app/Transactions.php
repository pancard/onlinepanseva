<?php

namespace App;

use Carbon\Carbon;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\Eloquent\Model;

class Transactions extends Model
{
    protected $table = "tbl_transactions";
    public $incrementing = false;
    public $timestamps = false;
    public $timezone = 'Asia/Kolkata';

    public function addTransaction($pan_id, $order_id, $first_name, $email, $mobile_number) {
        try {
            

            $transaction = $this;
            $transaction->pan_id = $pan_id;
            $transaction->order_id = $order_id;
            $transaction->first_name = $first_name;
            $transaction->email = $email;
            $transaction->mobile_number = $mobile_number;
            

            if ($transaction->save()) {
                return $transaction;
            }

            return false;
        } catch (QueryException $ex) {
            Log::error('Error while executing SQL Query', [
                'class' => 'Transactions',
                'function' => 'addTransaction',            
                'line_no' => $ex->getLine(),
                'error_message' => $ex->getMessage(),
            ]);

            return false;
        } catch (Exception $ex) {
            Log::error('Error while executing SQL Query', [
                'class' => 'Transactions',
                'function' => 'addTransaction',        
                'line_no' => $ex->getLine(),
                'error_message' => $ex->getMessage(),
            ]);

            return false;
        }
    }

    public function updateTransaction($payment) {
        try {
            
            $order_id = $payment['order_id'];
            
            $this->where('order_id', $order_id)->update([
                'payment_id' => $payment['id'],
                'status' => $payment['status'],
                'amount' => $payment['amount']/100,
                'method' => $payment['method'],
            ]);
                    
            return $order_id;
        } catch (QueryException $ex) {
            Log::error('Error while executing SQL Query', [
                'class' => 'Transactions',
                'function' => 'addTransaction',                
                'line_no' => $ex->getLine(),
                'error_message' => $ex->getMessage(),
            ]);

            return false;
        } catch (Exception $ex) {
            Log::error('Error while executing SQL Query', [
                'class' => 'Transactions',
                'function' => 'addTransaction',            
                'line_no' => $ex->getLine(),
                'error_message' => $ex->getMessage(),
            ]);

            return false;
        }
    }
    protected $fillable = [];


    protected $hidden = [];

    protected $casts = [];

    public function getTimestampAttribute($value)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $value, $this->timezone)->format('d/m/Y h:i:s A');
    }
}
