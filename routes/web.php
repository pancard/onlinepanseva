<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () { return view('index');});
Route::get('/index', function () { return view('index');});

Route::get('/required-proofs', function () { return view('pages.required-proofs');});
Route::get('/faq', function () { return view('pages.faq');});
Route::get('/contact', function () { return view('pages.contact');});
Route::get('/terms-and-conditions', function () { return view('pages.terms-and-conditions');});
Route::get('/privacy-policy', function () { return view('pages.privacy-policy');});
Route::get('/refund-policy', function () { return view('pages.refund-policy');});
Route::get('/disclaimer', function () { return view('pages.disclaimer');});
Route::get('/about', function () { return view('pages.about');});
Route::get('/makepayment', function () { return view('pages.makepayment');});	

Route::post('/update', 'DataController@store')->name('update');
Route::get('/chirag', 'PanController@chirag');

Route::get('/makepayment/{pan_id}', 'DataController@makepayment')->name('makepayment');

Route::post('/createorder', 'DataController@createOrder')->name('update');
Route::post('/updatetransaction', 'DataController@updateTransaction');
